=== LearnDash LMS ===
Author: LearnDash
Author URI: https://learndash.com 
Plugin URI: https://learndash.com
Slug: learndash-core
Tags: learndash
Requires at least: 5.0
Tested up to: 5.5.3
Requires PHP: 7.0
Stable tag: 3.3.0.2
Last Update: 2021-01-12

LearnDash LMS Plugin - Turn your WordPress site into a learning management system.

== Description ==

Turn your WordPress site into a learning management system.

Easily create & sell courses, deliver quizzes, award certificates, manage users, download reports, and so much more! By using LearnDash you have access to the latest e-learning industry trends for creating robust learning experiences.

See the [Features](https://www.learndash.com/wordpress-course-plugin-features/) page for more information.

== Installation ==

If the auto-update is not working, you always have the option to update manually. Please note, a full backup of your site is always recommended prior to updating. 

1. Deactivate and delete your current version of LearnDash LMS.
2. Download the latest version of LearnDash from our [support site](https://support.learndash.com/articles/my-downloads/).
3. Upload the zipped file via PLUGINS > ADD NEW, or to wp-content/plugins.
4. Activate the LearnDash LMS plugin via the PLUGINS menu.

== Changelog ==

= 3.3.0.2 =

* Added filter "learndash_use_wp_safe_redirect" to be used if a user is being redirected to the wp-admin when marking a lesson or course as complete
* Added warning when using an older PHP version
* Updated ReactDND library
* Fixed download quiz certificate link not showing when a quiz has an essay type question with "Not graded" status
* Fixed styling issue with Elementor and the [ld_profile] shortcode 
* Fixed subsequent pages do not respect the filter condition, for example when filtering completed courses only
* Fixed styling issue on the quiz question overview page where the second row isn't correctly aligned to the left
* Fixed warning when foreach() isn't run over an array
* Fixed unexpected token < in JSON when Elementor and LearnDash are activated
* Fixed essay bulk approval not working 
* Fixed essay/open type questions showing a default white space
* Fixed course grid block not saving the column numbers
* Fixed two buttons showing (publish and update) when reviewing a submitted essay in Gutenberg/the block editor 
* Fixed not being able to approve submitted essays 
* Fixed Video Pause on Window Unfocused causing YT videos to autoplay
* Fixed PHP notices
* Fixed matrix sorting view questions results not displaying matching answers correctly

= 3.3.0.1 = 

* Fixed E_PARSE error causing Error message: syntax error, unexpected "?"" In REST API v2 for PHP versions below 7.3 
* Fixed an issue where when loading the REST API endpoints metabox_init_filter caused problems with third-party plugins and groups
* Fixed users can�t upload assignments
* Fixed custom course label not applying to LD user status widget
* Fixed unsupported operand types
* Fixed deprecation warnings when using PHP 8

= 3.3.0 =

* Added LearnDash REST API v2 (beta)
* Added course export performance improvements
* Added quiz messages for the question answered state
* Updated quiz retry logic 
* Updated packages used in the builder
* Updated coding standards 
* Fixed quiz answer spacing 
* Fixed translations not updating 
* Fixed header distorted in focus mode
* Fixed marking a course complete via the profile page in the wp-admin
* Fixed course list in ld_profile shortcode not showing 
* Fixed missing latest quiz statistic 
* Fixed mark complete button cut off in focus mode
* Fixed lesson title not changing when using the_title filter
* Fixed group leaders not being able to filter assingments/essays 
* Fixed LearnDash blocks throwing a warning
* Fixed RTL breadcrumbs being the wrong direction
* Fixed PayPal recurring payments not enrolling users

View our full changelog [here](https://www.learndash.com/changelog/).

== Upgrade Notice ==

= 3.1.3 = 
Important security update: please update immediately.

== FAQ ==

= Do I need to update? =

It is always recommended to update. However given the nature of WordPress and the option to have many other plugins installed, custom code, etc. it is possible that a conflict would arise. This is why we always recommend testing the update on a development environment first. 

= Why am I getting an error notice when trying to update? =

If you are getting an error while trying to update your version of LearnDash LMS, verify that your license is still valid. 

Both your license key and email address should be entered via LEARNDASH LMS > SETTINGS > LMS LICENSE. You should then see a "Your license is valid" message appear. 

If not, you can find your correct information via our [Support Site](https://support.learndash.com/articles/my-downloads/).

If your license has expired, you can purchase a new one [here](https://www.learndash.com/pricing-and-purchase/).

= What will happen to my customizations when updating? =

As long as the customizations were not done directly in the core LearnDash plugin files, there should be no problem. We provide many template files and hooks for this purpose. 

