<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'prega' );

/** MySQL database username */
define( 'DB_USER', 'homestead' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p-j}L|YClWlrPc4:d4~ZzD->Gbb5hT<G*3K|Gp;[w%mAxK0;6(A0KZxjR*i}%{a$' );
define( 'SECURE_AUTH_KEY',  '[k%i.j,-RKyE-,lD8i{>8 &YG`.k0kW zwpck?2=ow}NTDitVe .sMuXp9OZ&/>]' );
define( 'LOGGED_IN_KEY',    '?Ni52rJG<nrb>:=(]Yc;C00L8,Q*/kLePY;T:@!.TrJu,Vce$FI@`TK5~p;bt_sG' );
define( 'NONCE_KEY',        'VHlYe_#-eGK+[]^U-5L!z)O7_km+mm1w3:[F*)@%MP&wHt776JL<_/,[#0HTg8mG' );
define( 'AUTH_SALT',        'j#5J}p-p` Bvo8BMm!B]jp^Dr?- bs+d36dW-O+Chx[:uM[*,WzZ1hb7(Kl;ukQ$' );
define( 'SECURE_AUTH_SALT', 'b:BqY;+5~2SQ2H9(dHG)JDAg1CY -ZoxCUDu,}G=UnJ519r;-=0ul+y7D[C{;WZi' );
define( 'LOGGED_IN_SALT',   '_*?1ErAp+3Tg)c--R.XBAuTqHdai`Z_QSZRYP(]_vTd:E*hAw<]9>c>NF3Z&1I4o' );
define( 'NONCE_SALT',       'S)Cy[=hER`:550$w-JbNmEZ;>*d^xb/Z_,Gt w.DfnSLj)^v<%D+u gM|Y73tJ7d' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
